﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Commun.Joueurs
{
    [Serializable]
    public class InfoJoueur
    {

        public string pseudo;
        public string guid;
        public int MMR;
        public int niveau;


        public InfoJoueur(string pseudo)
        {
            this.pseudo = pseudo;
        }

        public InfoJoueur()
        {
            
        }

        public void majInfoJoueur(InfoJoueur infoJoueur)
        {
            this.pseudo = infoJoueur.pseudo;
            this.guid = infoJoueur.guid;
            this.MMR = infoJoueur.MMR;
            this.niveau = infoJoueur.niveau;
        }

       
    }


}
