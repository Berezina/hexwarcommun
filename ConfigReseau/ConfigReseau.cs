﻿using System;
using System.Collections.Generic;
using System.Text;
using Commun.Menu;
using Commun.Joueurs;
using hexwarcommun.Menu;

namespace Commun.ConfigReseau
{
    public static class ConfigReseau
    {
        public const string EMPTY_STRING = "EMPTY";

        //Creation partie
        public const string DEMANDE_PARTIE_DISPO = "demandeGamesDispo";
        public const string REPONSE_PARTIE_DISPO = "recuGameDispo";
        public const string DEMANDE_CREER_PARTIE = "demandeCreerPartie";
        public const string REPONSE_CREER_PARTIE = "recuCreerPartie";

        //join || leave game
        public const string REJOINDRE_PARTIE_ACCEPTE = "rejoindreGameAccept";
        public const string REJOINDRE_PARTIE_REFUSE = "rejoindrePartieRefuse";
        public const string DEMANDE_REJOINDRE_PARTIE = "demandeRejoindreGame";
        public const string QUITTER_PARTIE = "quitterPartie";
        public const string PARTIE_SUPPRIMEE = "partieSupprime";

        //changement lobby
        public const string LOBBY_CHANGEMENT_INFO = "chegementLobby";
        public const string AJOUT_PERSONNE_LOBBY = "ajoutPersonneLobby";

        public const string DECONNEXION = "deconnexion";

        public const string AUTHENTIFICATION = "authentification";

        public static Type getTypeObject(string typeName)
        {
            try
            {

                switch (typeName)
                {
                    case "PartieLobbyMulti":
                        return typeof(PartieLobbyMulti);

                    case "String":
                        return typeof(String);

                    case "InfoJoueur":
                        return typeof(InfoJoueur);
                    case "Authentification":
                        return typeof(Authentification);
                    case "LobbyDisponible":
                        return typeof(LobbyDisponible);
                    case "InfoJoueurLobby":
                        return typeof(InfoJoueurLobby);


                }
            }
            catch
            {
                new Exception();

            }
            return null;
        }

        
    }
}
