﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using Commun.Menu;
using Newtonsoft.Json;
using System.Net.Sockets;
using System.Net;

namespace Commun.ConfigReseau
{
    class ObjetSocket
    {
        public Socket socket { get; set; }
        //public byte[] objectReseauData { get; set; }
        

        public ObjetSocket(Socket socket)
        {
            this.socket = socket;
        }

        public void envoyerObject(string objJson)
        {
            List<byte> data = new List<byte>();
            data.AddRange(BitConverter.GetBytes(objJson.Length)); // on donne la taille du packet avec les 4 premier bit (int32)
            data.AddRange(Encoding.Default.GetBytes(objJson)); // on donne l objet
            socket.Send(data.ToArray());
        }

        /*public void envoyerObjet(object obj)
        {
            
            
            creatObjetReseau(obj, obj.GetType().Name);
            sendData();
            resetData();
        }

        private void resetData()
        {
            objectReseauData = null;
        }

        public void creatObjetReseau(object obj, string type)
        {
            string str = JsonConvert.SerializeObject(obj);
            ObjetReseau objetReseau = new ObjetReseau(str, obj.GetType().Name);
            Serialize(objetReseau);
        }

        private string getJsonString(object obj)
        {
            string json = JsonConvert.SerializeObject(obj, Formatting.Indented);
            return json;
        }

        public void sendData()
        {
            socket.Send(objectReseauData);
        }

        private void Serialize(object serializeObject)
        {

            string json = string.Empty;
            var fullPacket = new List<byte>();
            json = getJsonString(serializeObject);
            fullPacket.AddRange(BitConverter.GetBytes(json.Length)); // on donne la taille du packet avec les 4 premier bit (int32)
            fullPacket.AddRange(Encoding.Default.GetBytes(json)); // on donne l objet
            objectReseauData = fullPacket.ToArray();

        }

        public static object Deserialize(byte[] dataToObject)
        {

            string readData = Encoding.Default.GetString(dataToObject);
            ObjetReseau objReseau = JsonConvert.DeserializeObject<ObjetReseau>(readData);

            return objReseau;
        }

        public static object Deserialize(ObjetReseau objetReseau)
        {

            Type typeObj = ConfigReseau.getTypeObject(objetReseau.typeObj);

            object obj = JsonConvert.DeserializeObject(objetReseau.obj,typeObj);
            return obj;
        }*/

    }
}
