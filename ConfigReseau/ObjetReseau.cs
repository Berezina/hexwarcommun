﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Commun.ConfigReseau
{
    [Serializable]
    public class ObjetReseau
    {
        public string obj;
        public string typeObj;
        public string typeRequete;

        public ObjetReseau(string obj, string typeObj, string typeRequete)
        {
            this.typeObj = typeObj;
            this.obj = obj;
            this.typeRequete = typeRequete;
        }

    }
}
