﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Commun.Menu
{
    public static class infoCreationPartie
    {
        public static Dictionary<int, string> dicoTypeDeMap = new Dictionary<int, string>()
        {
             {1,"Random"},
             {2, "Mediteranee"},
             {3,"Continentale"}
        };
        public static Dictionary<int, string> dicoTypeDePartieJoueur = new Dictionary<int, string>()
        {
             {1,"1v1"},
             {2, "2v2"},
             {3,"3v3"}
        };
        public static Dictionary<int, string> dicoTailleMap = new Dictionary<int, string>()
        {
             {1,"petite"},
             {2, "moyenne"},
             {3,"grande"},
            {4, "gigantesque"}
        };


    }
}
