﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Commun.Menu
{
    [Serializable]

    public class PartieLobbyMulti : PartieLobby
    {

        public string hostName;
        public string nomPartie;
        public string stringGuid;


        public PartieLobbyMulti(string nomPartie) : base()
        {
            
            this.nomPartie = nomPartie;
            
        }

        

        public void addInfoJoueurToList(InfoJoueurLobby infoJoueurLobby)
        {
            listInfoJoueur.Add(infoJoueurLobby);
        }

        

        public void creatGUID()
        {
            Guid guid = new Guid();
            guid = Guid.NewGuid();
            stringGuid = guid.ToString();
        }

        public void changerInfoJoueur(InfoJoueurLobby infoJoueur)
        {
            for(int i =0; i< listInfoJoueur.Count; i++)
            {
                if(infoJoueur.infoJoueur.guid == listInfoJoueur[i].infoJoueur.guid)
                {
                    listInfoJoueur[i] = infoJoueur;
                }
            }
        }
    }
}
