﻿using Commun.Joueurs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Commun.Menu
{
    [Serializable]
    public class PartieLobby
    {

        public List<InfoJoueurLobby> listInfoJoueur;
        public int typeEquipe;
        public int mapType;
        public int mapSize;

        public PartieLobby()
        {
            listInfoJoueur = new List<InfoJoueurLobby>();
            

        }

        public void removeJoueur(InfoJoueur infoJoueur)
        {
            foreach(InfoJoueurLobby infoJoueurLobby in listInfoJoueur)
            {
                if(infoJoueurLobby.infoJoueur.guid == infoJoueur.guid)
                {
                    listInfoJoueur.Remove(infoJoueurLobby);
                    break;
                }
            }
        }

    }
}
