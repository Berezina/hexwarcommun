﻿using Commun.Joueurs;
using System;
using System.Collections.Generic;
using System.Text;


namespace hexwarcommun.Menu
{
    [Serializable]
    public class Authentification
    {

        public InfoJoueur infoJoueur;
        public bool accepte;

        public Authentification(InfoJoueur infoJoueur)
        {
            this.infoJoueur = infoJoueur;
            accepte = false;
        }

        
    }
}
