﻿using Commun.Joueurs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Commun.Menu
{
    [Serializable]
    public class PartieLobbySolo : PartieLobby
    {
        public PartieLobbySolo(InfoJoueur infoJoueur) :base()
        {
            InfoJoueurLobby infoJoueurLobby = new InfoJoueurLobby(infoJoueur );
            this.listInfoJoueur.Add(infoJoueurLobby);
        }


    }
}
