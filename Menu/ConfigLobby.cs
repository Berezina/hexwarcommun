﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Commun.Menu
{
    public static class ConfigLobby
    {

        public static Dictionary<string, string> dicoCouleur = new Dictionary<string, string> // 1er str = nom , 2eme = hexadecimale de la couleur
        {
            { "Rouge", "EA0E00" },
            { "Bleu", "048DE0" },
            { "Orange", "EE6A00" },
            { "Jaune", "E89B07" },
            {"Vert","548E14" },
            {"Rose","E01B7F" },
            {"BleuFonce","000F93" },
            {"Gris","73787B" },
            {"Mauve","4B0267" },
        };

        private static string[] TAILLE_MAP = 
        {
            "small",
            "middle",
            "large",
            "huge"

        };

        private static string[] TYPE_MAP =
        {
            "continental",
            "mediterranean",
        };

        private static string[] TYPE_EQUIPE =
        {
            "1V1",
            "2V2",
            "3V3"

        };

        internal static string getTypeEquipe(int typeEquipe)
        {
            return TYPE_EQUIPE[typeEquipe];
        }

        internal static string getTypeMap(int mapType)
        {
            return TYPE_MAP[mapType];
        }

        internal static string getTailleMap(int mapSize)
        {
            return TAILLE_MAP[mapSize];
        }
    }
}
