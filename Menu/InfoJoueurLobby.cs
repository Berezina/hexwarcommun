﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Commun.Joueurs;


namespace Commun.Menu
{
    [Serializable]
    public class InfoJoueurLobby
    {
        public InfoJoueur infoJoueur;
        public ConfigJoueurs.COULEUR_JOUEUR couleur;
        public int nation;
        public int equipe;

        public InfoJoueurLobby(InfoJoueur infoJoueur)
        {
            this.infoJoueur = infoJoueur;
        }


    }
}
